/*
####### Copyright (c) 2024 Archivage Numérique ######################################################### @(°_°)@ #######
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
*/

package uuid

import (
	"strings"
	"testing"
)

func TestNew(t *testing.T) {
	ids := make(map[string]bool)

	for i := 0; i < 100000; i++ {
		id, err := New()
		if err != nil {
			t.Fatal(err) ///////////////////////////////////////////////////////////////////////////////////////////////
		}

		if ok := ids[id]; ok {
			t.Fatal("We have duplicates !") ////////////////////////////////////////////////////////////////////////////
		}

		if !IsValid(id) {
			t.Fatalf("This id is not valid: %s", id) ///////////////////////////////////////////////////////////////////
		}

		ids[id] = true
	}
}

func TestGenerate(t *testing.T) {
	_, err := generate(strings.NewReader(""))
	if err == nil {
		t.Fatal(`We should get an error with strings.NewReader("")`) ///////////////////////////////////////////////////
	}
}

func BenchmarkNew(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, _ = New()
	}
}

/*
####### END ############################################################################################################
*/
