/*
####### Copyright (c) 2024 Archivage Numérique ######################################################### @(°_°)@ #######
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
*/

package uuid

import (
	"crypto/rand"
	"encoding/hex"
	"io"
	"regexp"
)

var (
	_uuid = regexp.MustCompile(`^[\da-f]{8}-[\da-f]{4}-[\da-f]{4}-[\da-f]{4}-[\da-f]{12}$`)
)

func IsValid(id string) bool {
	return _uuid.MatchString(id)
}

func generate(reader io.Reader) (string, error) {
	var (
		bs [16]byte
		bd [36]byte
	)

	if _, err := io.ReadFull(reader, bs[:]); err != nil {
		return "", err
	}

	hex.Encode(bd[:8], bs[:4])
	hex.Encode(bd[9:13], bs[4:6])
	hex.Encode(bd[14:18], bs[6:8])
	hex.Encode(bd[19:23], bs[8:10])
	hex.Encode(bd[24:], bs[10:])

	bd[8] = '-'
	bd[13] = '-'
	bd[18] = '-'
	bd[23] = '-'

	return string(bd[:]), nil
}

func New() (string, error) {
	return generate(rand.Reader)
}

/*
####### END ############################################################################################################
*/
